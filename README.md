# mediaplay-discord.github.io

![license](https://img.shields.io/github/license/MediaPlay-Discord/mediaplay-discord.github.io?style=plastic) ![discord-server](https://img.shields.io/discord/699994812517974057?color=00b0f0&logo=MPD&style=plastic) ![uptime](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/MediaPlay-Discord/status/master/api/website/uptime.json&style=plastic)

This is the MediaPlay website source code,
feel free to make any improvements (such as grammar mistakes, typos, bug issue, etc.)
by making a pull request on this repository!

If you want to discuss about the MediaPlay website - and MediaPlay development in general - [join the Matrix room](https://matrix.to/#/#mediaplay-development:matrix.org). (you could also [join our Discord server](https://discord.gg/5Tdke6dsaP) as well, and you'll need to get through the gatekeeper, after that go to [#development-chat](https://discord.com/app/699994812517974057/788533905854300170))